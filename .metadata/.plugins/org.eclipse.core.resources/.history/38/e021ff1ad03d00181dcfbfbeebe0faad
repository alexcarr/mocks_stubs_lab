package tests;
import src.*;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.never;
import org.junit.Before;
import org.junit.Test;
 
public class LoginServiceTest {
   private IAccount account;
   private IAccountRepository accountRepository;
   private LoginService service;
 
   @Before
   public void init() {
      account = mock(IAccount.class);
      accountRepository = mock(IAccountRepository.class);
      when(accountRepository.find(anyString())).thenReturn(account);
      service = new LoginService(accountRepository);
   }
 
   private void willPasswordMatch(boolean value) {
      when(account.passwordMatches(anyString())).thenReturn(value);
   }
 
   @Test
   public void itShouldSetAccountToLoggedInWhenPasswordMatches() {
      willPasswordMatch(true);
      service.login("brett", "password");
      verify(account, times(1)).setLoggedIn(true);
   }
 
   @Test
   public void itShouldSetAccountToRevokedAfterThreeFailedLoginAttempts() {
      willPasswordMatch(false);
 
      for (int i = 0; i < 3; ++i)
         service.login("brett", "password");
 
      verify(account, times(1)).setRevoked(true);
   }
   
   @Test
   public void itShouldNotSetAccountLoggedInIfPasswordDoesNotMatch() {
      willPasswordMatch(false);
      service.login("brett", "password");
      verify(account, never()).setLoggedIn(true);
   }
   
   @Test
   public void itShouldNotRevokeSecondAccountAfterTwoFailedAttemptsFirstAccount() {
      willPasswordMatch(false);
 
      IAccount secondAccount = mock(IAccount.class);
      when(secondAccount.passwordMatches(anyString())).thenReturn(false);
      when(accountRepository.find("schuchert")).thenReturn(secondAccount);
 
      service.login("brett", "password");
      service.login("brett", "password");
      service.login("schuchert", "password");
 
      verify(secondAccount, never()).setRevoked(true);
   }
   
   @Test(expected = AccountLoginLimitReachedException.class)
   public void itShouldNowAllowConcurrentLogins() {
      willPasswordMatch(true);
      when(account.isLoggedIn()).thenReturn(true);
      service.login("brett", "password");
   }
   
   @Test(expected = AccountNotFoundException.class)
   public void ItShouldThrowExceptionIfAccountNotFound() {
      when(accountRepository.find("schuchert")).thenReturn(null);
      service.login("schuchert", "password");
   }
   
   @Test(expected = AccountRevokedException.class)
   public void ItShouldNotBePossibleToLogIntoRevokedAccount() {
      willPasswordMatch(true);
      when(account.isRevoked()).thenReturn(true);
      service.login("brett", "password");
   }
   //Cannot Login to Account with Expired Password
   @Test(expected = ExpiredPasswordException.class)
   public void ShouldNotBePossibleToLoginWithExpiredPassword(){
	   willPasswordMatch(true);
	   when(account.isLoggedIn()).thenReturn(true);
	   service.setPasswordExpired(true);
	   service.login("brett", "password");
   }
   
   //Can Login to Account with Expired Password After Changing the Password
   @Test
   public void ShouldBePossibleToLoginWithChangedExpiredPassword(){
	   willPasswordMatch(true);
	   service.setPasswordExpired(true);
	   service.setExpiredPasswordChanged(true);
	   service.login("schuchert", "password");
   }
   
   //Cannot Login to Account with Temporary Password
   @Test(expected = TempPasswordException.class)
   public void ShouldNotBeAbleToLoginWithTempPassword(){
	   willPasswordMatch(true);
	   service.setPasswordIsTemp(true);
	   service.login("schuchert", "password");
   }
   
   //
   @Test
   public void ShouldBeAbleToLoginWithTempPasswordAfterPasswordChange(){
	   willPasswordMatch(true);
	   service.setPasswordIsTemp(true);
	   service.setTempPasswordChanged(true);
	   service.login("schuchert", "password");
   }
   
   @Test(expected = RepeatedPasswordException.class)
   public void ShouldNotBeAbleToLoginIfNewPasswordIsOneOfTwentyFourMostRecentlyUsedPasswords(){
	   willPasswordMatch(true);
	   service.setNewPasswordIsRepeated(true);
	   service.login("schuchert", "password");
   }
   
   public void ShouldBeAbleToLoginIfPasswordIsOlderThanTwentyFourMostRecentlyUsedPasswords(){
	   willPasswordMatch(true);
	   service.setNewPasswordIsRepeated(false);
	   service.login("schuchert", "password");
   }
}