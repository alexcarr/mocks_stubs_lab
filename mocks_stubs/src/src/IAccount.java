package src;

public interface IAccount {
	   void setLoggedIn(boolean value);
	   boolean passwordMatches(String candidate);
	   void setRevoked(boolean value);
	   boolean isLoggedIn();
	   boolean isRevoked();
	   void changePassword(String newpassword);
	   boolean newPasswordIsWithinLastTwentyFourPasswords();
}
