package src;

public class LoginService {
	private final IAccountRepository accountRepository;
    private String previousAccountId = "";
    private boolean passwordExpired = false;
    private boolean expiredPasswordChanged = false;
    private boolean passwordIsTemp = false;
    private boolean tempPasswordChanged = false;
    private boolean newPasswordIsRepeated = false;
 
	public LoginService(IAccountRepository accountRepository) {
	   this.accountRepository = accountRepository;
	}
	public void setPasswordExpired(boolean value){
		passwordExpired = value;
	}
	public void setExpiredPasswordChanged(boolean value){
		expiredPasswordChanged = value;
	}
	public void setPasswordIsTemp(boolean value){
		passwordIsTemp = value;
	}
	public void setTempPasswordChanged(boolean value){
		tempPasswordChanged = value;
	}
	public void setNewPasswordIsRepeated(boolean value){
		newPasswordIsRepeated = value;
	}
	
	//returns true if the new password was one of the 24 previously used passwords.
	public boolean newPasswordIsRepeated(){
		return newPasswordIsRepeated;
	}
	 private int failedAttempts = 0;

	 public void login(String accountId, String password) {
	      IAccount account = accountRepository.find(accountId);
	      if (newPasswordIsRepeated)
	    	  throw new RepeatedPasswordException();
	      if (passwordIsTemp)
	    	  if(!tempPasswordChanged)
	    		  throw new TempPasswordException();
	      if (account == null)
	          throw new AccountNotFoundException();
	      if (passwordExpired)
	    	  if (!expiredPasswordChanged)
	    		  throw new ExpiredPasswordException();
	 
	      if (account.passwordMatches(password)) {
	    	  if (account.isLoggedIn())
	    		  throw new AccountLoginLimitReachedException();
	    	  if (account.isRevoked())
	              throw new AccountRevokedException();
	         account.setLoggedIn(true);
	      } else {
	         if (previousAccountId.equals(accountId))
	            ++failedAttempts;
	         else {
	            failedAttempts = 1;
	            previousAccountId = accountId;
	         }
	      }
	 
	      if (failedAttempts == 3)
	         account.setRevoked(true);
	   }
	 
}
