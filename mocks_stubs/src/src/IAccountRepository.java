package src;

public interface IAccountRepository {
    IAccount find(String accountId);
}
